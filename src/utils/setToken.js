import axios from 'axios';

const setToken = token => {
  if(token){
    axios.defaults.headers.common['Authorization'] = token;
    return true;
  } else {
    delete axios.defaults.headers.common['Authorization'];
    return false;
  }
};

export default setToken;