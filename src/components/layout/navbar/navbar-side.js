import React, { Component } from 'react';
import './style/navbar-side.scss'
import { Link } from 'react-router-dom';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../../actions/authActions';

class NavbarSide extends Component{

  logOut(e) {
    e.preventDefault();
    this.props.logoutUser();
  }

  render(){
    const { isAuthenticated, user } =  this.props.auth;

    const loggedOutLinks = [
      <Link to="/login" className="navbar__link--button">Log In</Link>,
      <Link to="/register" className="navbar__link--button">Register</Link>
    ]

    const loggedInLinks = [
      <Link to="/home" className="navbar__link--button">Home</Link>,
      <Link to="/settings" className="navbar__link--button">Settings</Link>,
      <a href="" onClick={this.logOut.bind(this)} className="navbar__link--button">Log Out</a>
    ]

    return (
      <div id="nav" className="navbar">
        <Link to="/" className="navbar__link--logo">Exchange Rate</Link>
        <Link to="/current-rates" className="navbar__link">Current Rates</Link>
        <Link to="/time-span" className="navbar__link">Time Span</Link>
        <Link to="/by-currency" className="navbar__link">By Currency</Link>
        <div className="navbar__link--group">
        { isAuthenticated ? loggedInLinks : loggedOutLinks}
        <Link to="/" className="navbar__link--button">About</Link>
        </div>
      </div>
    );
  }
}

NavbarSide.protoTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
 auth: state.auth
});

export default connect(mapStateToProps, { logoutUser })(NavbarSide);