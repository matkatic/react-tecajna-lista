import React, { Component } from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { isEmpty } from './../../../../utils/isEmpty';
// import { withRouter } from "react-router";

import { loginUser } from './../../../../actions/authActions';

import './style/login.scss';

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      auth: {},
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillUpdate(nextProps, nextState){
    console.log(nextProps);
    if(nextProps.auth.isAuthenticated){
      console.log(this.props);
      this.props.history.push("/home");
    }
 }
 
  onSubmit(e) {
    e.preventDefault();
    
    const user = {
      email: this.state.email,
      password: this.state.password
    };
    
    this.props.loginUser(user);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { errors } = this.props
   

    return (
      <div className="login">
        <h1 className="login__title">Log In</h1>
        <form onSubmit={this.onSubmit}>
          <div className="form__group">
            <input type="email" placeholder="Email Address" name="email" value={this.state.email} onChange={this.onChange}
              className={classnames('form__input', {'form__input--invalid': errors.email})} />
              {errors.email && ( <div className="form__input--errors">{errors.email}</div> )}
          </div>
          <div className="form__group">
            <input type="password" placeholder="Password" name="password" value={this.state.password} onChange={this.onChange}
              className={classnames('form__input', {'form__input--invalid': errors.password})} />
              {errors.password && ( <div className="form__input--errors">{errors.password}</div> )}
          </div>
          <input type="submit" className="btn--submit" />
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});


export default connect(mapStateToProps, { loginUser })(Login);
