import React, { Component } from 'react';
import axios from 'axios';
import classnames from 'classnames';
import { connect } from 'react-redux';
import { registerUser } from './../../../../actions/authActions';
import PropTypes from 'prop-types';
import './style/register.scss';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      email: '',
      password: '',
      passwordConfirmation: '',
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();

    const userData = {
      name: this.state.name,
      email: this.state.email,
      password: this.state.password,
      password_confirmation: this.state.passwordConfirmation
    };
    this.props.registerUser(userData)    
  }

  render() {
    const { errors } = this.props;

    return (
      <div className="register">
        <h1 className="register__title">Sign Up</h1>
          <form noValidate onSubmit={this.onSubmit}>
            <div className="form__group">
              <input type="text" placeholder="Name" name="name" value={this.state.name} onChange={this.onChange}
                className={classnames('form__input', {'form__input--invalid': errors.name})} />
              {errors.name && <div className="form__input--errors">{errors.name}</div> }
            </div>
            <div className="form__group">
              <input type="email" placeholder="Email Address" name="email" value={this.state.email} onChange={this.onChange}
                className={classnames('form__input', { 'form__input--invalid': errors.email })} />
              {errors.email && ( <div className="form__input--errors">{errors.email}</div> )}
            </div>
            <div className="form__group">
              <input type="password" placeholder="Password" name="password" value={this.state.password} onChange={this.onChange}
                className={classnames('form__input', { 'form__input--invalid': errors.password })} />
              {errors.password && ( <div className="form__input--errors">{errors.password}</div> )}
            </div>
            <div className="form__group">
              <input type="password" placeholder="Confirm Password" name="passwordConfirmation" value={this.state.passwordConfirmation} onChange={this.onChange}
                className={classnames('form__input', { 'form__input--invalid': errors.password_confirmation })} />
              {errors.password_confirmation && ( <div className="form__input--errors">{errors.password_confirmation}</div> )}
            </div>
            <input type="submit" className="btn--submit" />
          </form>
        </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(Register);
