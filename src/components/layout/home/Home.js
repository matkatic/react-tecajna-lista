import React, { Component } from 'react';
import axios from 'axios';
import './style/home.scss';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { getCurrencyByDate } from './../../../actions/currencyActions';
import { isEmpty } from '../../../utils/isEmpty';


class Home extends Component {
  constructor() {
    super();
    this.state = {}

    this.getData = this.getData.bind(this);
  }

  getData(){
    console.log("Get Data");
    let currency = "USD,EUR";
    let date = "21-09-2018";
    this.props.getCurrencyByDate(date, currency);
  }
 
  render() {
    const { isAuthenticated, user } =  this.props.auth;
    
    const createUserSettings = [
      <h3>You have no active currency settings.</h3>

    ];

    return (
      <div className="home">
        <div className="home__group--top">
          <h1 className="home__title--main">Home page</h1>
          <h3 className="home__title--sub">This page shows selected currencies and their values.</h3>
        </div>
        <div className="home__group--middle">
          { isEmpty(user) > 0 ? createUserSettings : <button type="submit" onClick={this.getData()}>Submit</button> }
        </div>        
      </div>
    );
  }
}

Home.propTypes = {
  getCurrencyByDate: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
 auth: state.auth,
 byDate: state.byDate
});

export default connect(mapStateToProps, { getCurrencyByDate })(Home);
