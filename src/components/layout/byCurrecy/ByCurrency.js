import React, { Component } from 'react';
import axios from 'axios';
import './style/byCurrency.scss';
import { connect } from 'react-redux';
import { getCurrencyByDate } from '../../../actions/currencyActions';
import { PropTypes } from 'prop-types';

class ByCurrency extends Component {
  constructor() {
    super();
    this.state = {}
    console.log(this.props);
  }
 
  render() {
    return (
      <div className="byCurrency">
        <h2>ByCurrency!</h2>
      </div>
    );
  }
}

ByCurrency.propTypes = {
  getCurrencyByDate: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
 auth: state.auth
});

export default connect(mapStateToProps, { getCurrencyByDate })(ByCurrency);
