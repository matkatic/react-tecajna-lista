import React, { Component } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';

import { getCurrentRate } from '../../../actions/currencyActions';

import './style/currentRates.scss';


class CurrentRates extends Component {
  constructor() {
    super();
    this.state = {}
  }
 
  render() {
    return (
      <div className="currentRates">
        <h2>CurrentRates!</h2>
      </div>
    );
  }
}

CurrentRates.propTypes = {
  getCurrencyByDate: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
}

const mapStateToProps = (state) => ({
 auth: state.auth
});

export default connect(mapStateToProps, { getCurrentRate })(CurrentRates);
