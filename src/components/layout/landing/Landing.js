import React, { Component } from 'react';
import './style/landing.scss'

class Landing extends Component{
  render(){
    return (
      <div>
        <div className="landing"></div>
        <span className="message"><h1>Your trusted financial advisor</h1></span>        
      </div>
    );
  }
}

export default Landing;