import Axios from 'axios';
import { GET_ERRORS, GET_CURRENCY_BY_DATE } from './types';


export const getCurrencyByDate = (dateOfCurrency, currenciesArray) => dispatch => {
  Axios
  .get('/exchange-rate/get-by-date', {
        params: {
           date: dateOfCurrency,
           currencies: currenciesArray}
        }
      )
  .then(res => { 
    if (res.data.errors) {
      console.log(res.data.errors);
      dispatch({
        type: GET_ERRORS,
        data: res.data.errors
      });
    } else {
      console.log(res.data);
      dispatch({
        type: GET_CURRENCY_BY_DATE,
        data: res.data
      });
    }
  });
}

export const getCurrentRate = () => dispatch => {
  return true;
}

export const getHomePageData = () => dispatch => {
  return true;
}