import Axios from 'axios';
import setToken from './../utils/setToken';

import { GET_ERRORS, SET_CURRENT_USER, LOG_OUT_USER } from './types';

// User registration
export const registerUser = userData => dispatch => {
  Axios
    .post('/register', userData)
    .then(res => { 
      if (res.data.errors) {
        console.log(res.data.errors);
        dispatch({
          type: GET_ERRORS,
          data: res.data.errors
        });
      } else {
        console.log(res.data);
        let token = res.data.access_token;
        localStorage.setItem('railsToken', token); 
        setToken(token);
        console.log(localStorage);
        dispatch(setCurrentUser(res.data));
      }

    })
};

// Get user token
export const loginUser = (userData) => dispatch => {
  Axios
  .post('/login', userData)
  .then(res => {
    if(res.data.errors){
      dispatch({
        type: GET_ERRORS,
        data: res.data.errors
      });
    }
    else
    {
      let token = res.data.access_token;
      localStorage.setItem('railsToken', token); 

      if (setToken(token)) 
      {
        delete res.data.access_token;
        delete res.data.token_type;
        dispatch(setCurrentUser(res.data))
      }
      else 
      {
          dispatch({
            type: GET_ERRORS,
            data: { password: "User not logged in! Please try again." }
          })
      }
    }
  })
}

export const setCurrentUser = (user) => {
    return {
      type: SET_CURRENT_USER,
      data: user
    }
}

export const logoutUser = () => dispatch => {
  localStorage.removeItem('railsToken');
  setToken();
  dispatch({
    type: LOG_OUT_USER,
    data: {}
  });
}