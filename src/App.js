import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import axios from 'axios';

// COMPONENTS
import NavbarSide from './components/layout/navbar/navbar-side';
import Landing from './components/layout/landing/Landing';
import Login from "./components/layout/authentication/Login/Login";
import Register from "./components/layout/authentication/Register/Register";
import Home from './components/layout/home/Home';
import ByCurrency from './components/layout/byCurrecy/ByCurrency';
import CurrentRates from './components/layout/currentRates/CurrentRates';
import TimeSpan from './components/layout/timeSpan/TimeSpan';
import Settings from './components/layout/settings/Settings';

// STYLE AND MISC
import './App.scss';


axios.defaults.baseURL = 'http://localhost:3001/api';
axios.defaults.headers.post['Content-Type'] = 'application/json';


class App extends Component {
  render() {
    return (
      <Provider store={ store }>
        <Router>
          <div className="App">
            <div className="App__navigation">
              <NavbarSide />
            </div>
            <div className="App__content">
              <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/home" component={Home} />

                <Route exact path="/register" component={Register} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/settings" component={Settings} />

                <Route exact path="/time-span" component={TimeSpan} />
                <Route exact path="/current-rates" component={CurrentRates} />
                <Route exact path="/by-currency" component={ByCurrency} />
              </Switch>
            </div>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
