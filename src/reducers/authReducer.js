import { SET_CURRENT_USER, LOG_OUT_USER } from "../actions/types";

const initialState = {
  isAuthenticated: false,
  user: {}
}
export default function(state = initialState, action) {
  switch(action.type){
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: true,
        user: action.data
      }
    case LOG_OUT_USER:
      return initialState;
    default:
      return state;
  }
}