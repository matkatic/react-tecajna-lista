import { GET_BY_DATE, GET_CURRENT_RATE, GET_HOME_PAGE_DATA } from "../actions/types";

export default function(state = initialState, action) {
  switch(action.type){
    case GET_HOME_PAGE_DATA:
      return {
        ...state,
        homePage: action.data
      }
    case GET_CURRENT_RATE:
      return{
        ...state,
        currentRate: action.data
      }
    case GET_BY_DATE:
      return {
        ...state,
        byDate: action.data
      }
    default:
      return state;
  }
}